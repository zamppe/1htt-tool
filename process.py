import struct

import pymem
from pymem.exception import ProcessNotFound


class Process(pymem.Pymem):
    def __init__(self) -> None:
        super().__init__()
        self.base = None

    def __del__(self):
        self.close_process()

    def open_process_from_name(self, process_name):
        try:
            super().open_process_from_name(process_name)
            self.base = self.process_base.lpBaseOfDll
        except ProcessNotFound:
            self.base = None

    @property
    def is_open(self) -> bool:
        # Hacky util
        if not self.base:
            return False
        try:
            self.read_bytes(self.base + 0x87050, 1000)
            return True
        except Exception:
            return False

    @property
    def base_hex(self) -> str:
        return f'0x{self.process_base.lpBaseOfDll:X}'

    def open_process_from_id(self, process_id):
        # Overriding this in order to disable debug
        if not process_id or not isinstance(process_id, int):
            raise TypeError('Invalid argument: {}'.format(process_id))
        self.process_id = process_id
        # noinspection PyTypeChecker
        self.process_handle = pymem.process.open(self.process_id, debug=False)
        if not self.process_handle:
            raise pymem.exception.CouldNotOpenProcess(self.process_id)

    def read_address(self, address, fmt):
        rawdata = self.read_bytes(address, struct.calcsize(fmt))

        result = struct.unpack(fmt, rawdata)

        return result if len(result) > 1 else result[0]


    def read_relative_address(self, address, fmt):
        return self.read_address(self.base + address, fmt)

EOL = Process()

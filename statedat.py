from process import EOL
from utils import *


def statedat_address():
    return EOL.read_relative_address(0x87050, 'q')


def top10s():
    top10s = []
    address = statedat_address() + 4

    for _ in range(54):
        times = EOL.read_address(address, '10i')
        address += 40

        players = []
        for _ in range(10):
            player = EOL.read_address(address, '8s')
            player = byte_literal_to_str(player)

            players.append(player)
            address += 15

        address += 498
        top10 = [(t * 10, p) for t, p in zip(times, players) if t > 0]
        top10s.append(top10)

    return top10s


def player_bests():
    player_bests = []
    for top10 in top10s():
        top10_bests = {}
        for t, p in top10:
            if p not in top10_bests:
                top10_bests[p] = t

        player_bests.append(top10_bests)

    return player_bests


def players():
    address = statedat_address() + 61920
    num_players = EOL.read_address(address + 116 * 50, 'i')
    players = []

    for _ in range(num_players):
        player = byte_literal_to_str(EOL.read_address(address, '8s'))
        address += 116
        players.append(player)

    return players


def active_player():
    address = statedat_address() + 61920

    return byte_literal_to_str(EOL.read_address(address + 116 * 50 + 4, '8s'))


def time_penalty():
    return 60 * 10 * 1000


def total_times():
    total_times = {player: 54 * time_penalty() for player in players()}
    for p_bests in player_bests():
        for player, time in p_bests.items():
            total_times[player] -= (time_penalty() - time)

    return total_times


def total_times_human():
    return {p: millis_to_str(t) for p, t in total_times().items()}


def total_time_of_player(player):
    tts = total_times_human()

    if player not in tts:
        return millis_to_str(time_penalty() * 54)

    return total_times_human()[player]


def times_of_player(player):
    player_times = []
    for p_bests in player_bests():
        if not player in p_bests:
            player_times.append(time_penalty())
        else:
            player_times.append(p_bests[player])

    return [millis_to_str(time) for time in player_times]

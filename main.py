import time

import pygame
from pygame.locals import *

from constants import *
from process import EOL
from statedat import active_player, times_of_player, total_time_of_player
from timer import Timer
from utils import *
from window import Window


def main():
    window = Window(300, 570, '1htt')
    window_fps = 20
    key_timer_start = '6'
    key_timer_reset = '7'
    timer = Timer(3600)
    times = []

    memory_read_interval = 5.0
    time_since_memory_read = memory_read_interval + 1.0
    now = time.time()

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                return

            if event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()

        if is_key_pressed(ord(key_timer_start)):
            timer.start()
        if is_key_pressed(ord(key_timer_reset)):
            timer.reset()

        prev = now
        now = time.time()
        dt = now - prev
        timer.update(dt)
        time_since_memory_read += dt

        if time_since_memory_read >= memory_read_interval:
            time_since_memory_read = 0.0

            if not EOL.is_open:
                EOL.open_process_from_name('eol.exe')

            if EOL.is_open:
                tt = total_time_of_player(active_player())
                times = times_of_player(active_player())
                times = [(t, RED if t[0] == '1' else GREEN) for t in times]

            else:
                tt = 'EOL do\'nt runig !!'

        window.clear()
        window.render_title(str(timer))
        window.render_title2(tt)
        window.render_columns(times)
        window.flip()

        time.sleep(1 / window_fps)


if __name__ == '__main__':
    main()

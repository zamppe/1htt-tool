import win32api

def millis_to_str(hundreths):
    seconds = hundreths // 1000 % 60
    minutes = hundreths // (1000 * 60) % 60
    hours = hundreths // (1000 * 60 * 60)

    if hundreths >= 1000 * 60 * 60:
        return f'{hours:02}:{minutes:02}:{seconds:02},{hundreths // 10 % 100:02}'

    return f'{minutes:02}:{seconds:02},{hundreths // 10 % 100:02}'

def is_key_pressed(key):
    #"if the high-order bit is 1, the key is down; otherwise, it is up."
    return (win32api.GetKeyState(key) & (1 << 7)) != 0


def byte_literal_to_str(byte_literal):
    return byte_literal.decode('windows-1252').lstrip('\x00').split('\x00', 1)[0]

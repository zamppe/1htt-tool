from utils import millis_to_str

class Timer:
    def __init__(self, seconds):
        self.millis = seconds * 1000
        self.initial_millis = self.millis
        self.running = False


    def is_running(self):
        return self.running


    def toggle(self):
        self.running = not self.running


    def start(self):
        self.running = True


    def stop(self):
        self.running = False


    def update(self, dt):
        if not self.is_running():
            return

        millis = int(dt * 1000)
        self.millis -= millis


    def reset(self):
        self.stop()
        self.millis = self.initial_millis


    def __repr__(self):
        return self.__str__()


    def __str__(self):
        return millis_to_str(self.millis)

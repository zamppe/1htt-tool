import pygame

from constants import *


class Window:
    def __init__(self, w, h, caption):
        self.w = w
        self.h = h
        self.window = pygame.display.set_mode((w, h))
        pygame.display.set_caption(caption)

        pygame.font.init()
        self.title_font = pygame.font.SysFont('arial', 30)
        self.columns_font = pygame.font.SysFont('arial', 18, bold=True)

    def render_title(self, text):
        surface = self.title_font.render(text, 1, GREEN)
        self.window.blit(surface, ((self.w - surface.get_width()) // 2, 0))

    def render_title2(self, text):
        surface = self.title_font.render(text, 1, GREEN)
        self.window.blit(surface, ((self.w - surface.get_width()) // 2, 35))

    def render_columns(self, texts):
        half = len(texts) // 2
        for i, (text, color) in enumerate(texts):
            row = i % half
            col = i // half
            color = (255, 0, 0) if text[0] == '1' else (0, 255, 0)
            s = self.columns_font.render(f'{i + 1:2}: {text}', 1, color)
            self.window.blit(s, (col * 150 + 25, row * 18 + 70))

    def clear(self):
        pygame.display.get_surface().fill((0, 0, 0))

    def flip(self):
        pygame.display.flip()
